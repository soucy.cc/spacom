
Biographie
==========

Depuis que je suis toute petite, ce qui me procure le plus grand des
plaisirs c’est le dessin. Surtout la création de personnages. J’arrive
à peine à comprendre comment ils atterrissent au bout de mon crayon!

## Listes de client

La Pastèque, Magazine chickaDEE, La Courte Échelle, Magazine Enfants
Québec, Magazine Histoire pour les petits, Magazine J’aime lire,
Magazine Mes premiers J’aime lire, Magazine Astrapi

## Prix et mentions

* Bourse d’illustration Michèle Lemieux 2011
* Prix LUX en illustration, pièce unique, étudiant 2011
* Prix illustration jeunesse du Salon du livre de Trois-Rivières,
  2013, catégorie Petit roman illustré, Sucredor, Une créature
  inattendue, éditions La courte échelle

![](/biographie-spa.jpg)

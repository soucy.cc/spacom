
--[[
  Satelito website configuration file.
  <https://soucy.cc/git/satelito/file/README.md.html>
]]

return {
  --[[
    App Mandatory Values
    (If you edit them do it carefully)

    - siteurl
    - language
    - paths.*
    - mimetypes.*
  ]]
  sitemapxml = true,
  siteurl = "https://sophiepa.com",
  language = "fr",

  -- Main paths
  -- Must be relative paths to the config.lua
  paths = {
    content = 'content/', -- where are the source files (markdown, lua)
    templates = 'templates/', -- where are the etlua templates
    public_html = 'public_html/', -- where you export your site
    -- You can add yours here
    portfolio_images = 'public_html/images/portfolio/'
  },

  -- Accepted mime types of the non-textual content
  mimetypes = {
    'image/avif',
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
    'image/webp',
    'application/pdf',
  },

  --[[
    Templates specific values
  ]]
  metas = {
    generator = "Satelito",
  },

  -- Author infos
  author = {
    name = "Sophie PA",
    uri = "https://sophiepa.com/#",
    relme = {
      "https://www.facebook.com/sophiepa.illustratrice",
      "http://www.linkedin.com/in/sophiepa",
      "https://www.behance.net/sophiepa",
      "https://vimeo.com/user31928447"
    },
  },

  -- Images
  srcset = { '320', '480', '768', '1024' },
}

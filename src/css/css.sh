#!/bin/sh
# You can use it with entr to rebuild on change:
# find . | entr -d ./css.sh

dst="../../public_html/css/"

# Top bundle
# Return top.min.css in $dst
while read -r line; do
  [ -n "$line" ] && cat "./$line"
done <<-EOF | cssmin > "${dst}top.min.css"
utilities.css
base.css
logo.css
nav.css

EOF

# Bottom bundle
# Return bottom.min.css in $dst
while read -r line; do
  [ -n "$line" ] && cat "./$line"
done <<-EOF | cssmin > "${dst}bottom.min.css"

EOF

# Modular
cssmin nav-pagination.css > "${dst}nav-pagination.min.css"
cssmin nav-sequential.css > "${dst}nav-sequential.min.css"
cssmin portfolio-list.css > "${dst}portfolio-list.min.css"
cssmin portfolio-image.css > "${dst}portfolio-image.min.css"

echo "CSS builded! @ $(date +"%Y-%m-%d %T")"

#!/bin/sh

# You can use it with entr to rebuild on change:
# find . | entr -d ./js.sh

# Dependencies:
# - jsmin (clang)

dst="../../public_html/js/"

# Bottom bundle
# Return top.min.css in $dst
while read -r line; do
  [ -n "$line" ] && cat "./$line"
done <<-EOF | jsmin > "${dst}bottom.min.js"
nav-sequential.js

EOF



echo "JS builded! @ $(date +"%Y-%m-%d %T")"

(function () {
  'use strict';

  var relnext = document.querySelector('a[rel*="next"]');
  var relprev = document.querySelector('a[rel*="prev"]');
  var relup = document.querySelector('a[rel*="up"]');
  var pgIndex = document.querySelector('#pgIndex');
  var pgIndexGo = document.querySelector('#pgIndexGo');

  // Navigate through portfolio's pages with left and right arrows
  document.addEventListener('keydown', function(evt) {
    if (evt.keyCode == '37' && relprev) {
      relprev.click();
    } else if (evt.keyCode == '39' && relnext) {
      relnext.click();
    } else if (evt.altKey && evt.keyCode == '38' && relup) {
      relup.click();
    }
  });

  if (pgIndex && pgIndexGo) {
    pgIndexGo.addEventListener('click', function(evt) {
      if (pgIndex.value !== '1') {
        window.location.pathname = '/pg-'+pgIndex.value+'.html';
      } else {
        window.location.pathname = '/';
      }

      return;
    });
  }
})();
